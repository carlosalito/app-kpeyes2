import { PerguntasProvider } from './../providers/perguntas/perguntas';
import { QuestionariosProvider } from './../providers/questionarios/questionarios';
import { ServiceProvider } from './../providers/service/service';
import { RestaurantesProvider } from './../providers/restaurantes/restaurantes';
import { DatabaseProvider } from './../providers/database/database';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage'
import { ListPage } from '../pages/list/list';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'LoginPage';

  pages: Array<{title: string, component: any}>;
  listaIcones : Array<string>;
  listaCores : Array<string>;
  loading : any;

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public dbService: DatabaseProvider,
    public restService: RestaurantesProvider,
    public questService: QuestionariosProvider,
    public pergutaService: PerguntasProvider,
    public service: ServiceProvider,
    public storage: Storage,
    public menuCtrl : MenuController
  ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: 'HomePage' },
      { title: 'Questionários', component: 'RestaurantesPage' },
      { title: 'Questionarios não transmitidos', component: ListPage }
    ];

    this.listaIcones = ['home','paper'];
    this.listaCores = ['primary','corTeste'];

  }

  initializeApp() {
    this.menuCtrl.swipeEnable(false);
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.dbService.createDatabase();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  sincronizar() {
    this.loading = this.service.createLoading('Sincronizando...');
    this.sincRestaurantes();      
  }

  sincRestaurantes() {
    this.restService.getRestaurantesAPI().subscribe(
      data => {
        let restaurantes : any;
        restaurantes = data.body;

        if (restaurantes.length > 0) {
          for (let i = 0; i < restaurantes.length; i++) {
            const restaurante = restaurantes[i];
            this.restService.insert(restaurante);
          }
        }
        
        this.sincQuestionarios();
      },
      err => {
        this.loading.dismiss();
        this.service.alerta('Erro ao sincronizar restaurantes', 'toast-error');
        console.log(err);          
      },
      () => {        
      }
    );
  }

  sincQuestionarios() {
    this.questService.getQuestionarioApi().subscribe(
      data => {
        let questionarios : any;
        questionarios = data.body;

        if (questionarios.length > 0) {
          for (let i = 0; i < questionarios.length; i++) {
            console.log(questionarios[i]);
            
            this.questService.insert(questionarios[i]).then(() => {
                if (i == questionarios.length - 1) {
                  this.sincPerguntas();
                }
            });
          }

          //aqui tu chama a função das perguntas
        }
      },
      err => {
        this.loading.dismiss();
        this.service.alerta('Erro ao sincronizar questionários', 'toast-error');
        console.log(err);          
      },
      () => {        
      }
    );
  }


  insertOpcoes(id, opcoes) {
    return this.pergutaService.insertOpcoes(id, opcoes)
    .then(() => {
      return true;
    })
    .catch((e) => {
      console.error(e)
      this.loading.dismiss();
    })
  }

  sincPerguntas() {
    this.pergutaService.getPerguntaApi().subscribe(
      data => {
        let perguntas : any;
        perguntas = data.body;

        if (perguntas.length > 0) {
          for (let i = 0; i < perguntas.length; i++) {
            const pergunta = perguntas[i];
            
            let respostaOpcao = null;
            if (pergunta.respostas) {
              const respostasOpcoes = JSON.parse(pergunta.respostas);
              respostaOpcao = respostasOpcoes.resposta;
            }

            this.pergutaService.insert(pergunta)
            .then(()=>{
              if (respostaOpcao) {
                this.insertOpcoes(pergunta.id, respostaOpcao).then(() => {
                    if (i == perguntas.length - 1) {
                      this.service.alerta('Dados sincronizados com sucesso!', 'toast-success');
                      this.loading.dismiss();
                    }
                })
                .catch((e) => {
                  console.error(e);
                 // this.loading.dismiss();
                });
              } else {
                if (i == perguntas.length - 1) {
                  this.service.alerta('Dados sincronizados com sucesso!', 'toast-success');
                  this.loading.dismiss();
                }
              }
            })
            .catch((e) => {
              console.error(e);
              this.loading.dismiss();
            })
          }          
        }
      },
      err => {
        this.loading.dismiss();
        this.service.alerta('Erro ao sincronizar questionários', 'toast-error');
        console.log(err);          
      },
      () => {       
      }
    );
  }

  logOff() {
    this.storage.remove('usuario');
    this.nav.setRoot('LoginPage')
  }


}
