import { DatabaseProvider } from './../database/database';
import { ServiceProvider } from './../service/service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class RestaurantesProvider {

  constructor(
    public http: HttpClient,
    public dbService: DatabaseProvider,
    public service: ServiceProvider
  ) {
    console.log('Hello RestaurantesProvider Provider');
  }

  public insert(restaurante) {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
      let sql = 'replace into restaurantes (sigla, nome) values (?, ?)';
      let dados = [restaurante.sigla, restaurante.nome];

      return db.executeSql(sql, dados)
      .catch((e) => console.error(e));  

    })
    .catch((e) => console.error(e));  
  }

  public getAll() {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
      let sql = 'select * from restaurantes';
      let dados = [];

      return db.executeSql(sql, dados)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let restaurantes = [];
            for (let i = 0; i < data.rows.length; i++) {
              const restaurante = data.rows.item(i);
              restaurantes.push(restaurante);
            }
            return restaurantes;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));  

    })
    .catch((e) => console.error(e)); 
  }


  getRestaurantesAPI() {
    return this.http.get(this.service.servidor + 'json&format=json&Itemid=805', {observe: 'response'});
  }

}
