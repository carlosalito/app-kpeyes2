import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoadingController, Loading, ToastController } from 'ionic-angular';


@Injectable()
export class ServiceProvider {

  servidor = 'https://arcos.kpeyes.io/index.php?option=com_json&view=';


  constructor(
    public http: HttpClient,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController
  ) {
    console.log('Hello ServiceProvider Provider');
  }

  createLoading(mensagem: string) : Loading {
    const loading: Loading = this.loadingCtrl.create({
      content : mensagem,
      spinner: 'bubbles',
    });

    loading.present();

    return loading;
  }

  alerta(msg, css) {
    const toast = this.toastCtrl.create({
    message: msg,
    duration: 3000,
    cssClass: css
    });
    toast.present();
  }

}
