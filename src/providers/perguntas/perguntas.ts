import { DatabaseProvider } from './../database/database';
import { ServiceProvider } from './../service/service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class PerguntasProvider {

  constructor(public http: HttpClient,
    public service: ServiceProvider, 
    public dbService: DatabaseProvider   
  ) {
    console.log('Hello PerguntasProvider Provider');
  }

 /*
  1 - texto
  2 - numero
  3 - marcação
  4 - opção
  */

  public insert(pergunta) {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
      let sql = 'replace into perguntas (idApi, idQuest, sigla, pergunta, peso, tipo) values (?, ?, ?, ?, ?, ?)';
      let dados = [pergunta.id, pergunta.id_questionario, 
        pergunta.sigla, pergunta.pergunta, 
        pergunta.peso, pergunta.tipo];

      return db.executeSql(sql, dados)
      .catch((e) => console.error(e));  

    })
    .catch((e) => console.error(e));  
  }

  public insertOpcoes(id, respostas) {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
      let sql = 'replace into opcoesPerguntas (idPergunta, opcao) values (?, ?)';
      let dados = [id, respostas];

      return db.executeSql(sql, dados)
      .catch((e) => console.error(e));  

    })
    .catch((e) => console.error(e));  
  }

  public getQuestionarioPorId(id) {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
      let sql = 'select t1.*, t2.opcao from perguntas AS t1 left join opcoesPerguntas AS t2 ON (t2.idPergunta = t1.idApi) where t1.idQuest = ?';
      let dados = [id];

      return db.executeSql(sql, dados).then((data : any) => {
        if (data.rows.length > 0) {
          let perguntas = [];

          for (let i = 0; i < data.rows.length; i++) {
            const pergunta = data.rows.item(i);
            let opcao = pergunta.opcao;
            if (opcao) {
              opcao = opcao.split(',');
              pergunta.opcao = opcao;
            }

            perguntas.push(pergunta);
          }

          return perguntas;
        } else {
          return [];
        }
      })
      .catch((e) => console.error(e));  

    })
    .catch((e) => console.error(e)); 
  }

  public getAll() {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
      let sql = 'select * from perguntas';
      let dados = [];

      return db.executeSql(sql, dados)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let perguntas = [];
            for (let i = 0; i < data.rows.length; i++) {
              const pergunta = data.rows.item(i);
              perguntas.push(pergunta);
            }
            return perguntas;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));  

    })
    .catch((e) => console.error(e)); 
  }

  getPerguntaApi() {
    return this.http.get(this.service.servidor + 'json&format=json&Itemid=779', {observe: 'response'});  
  }

}
