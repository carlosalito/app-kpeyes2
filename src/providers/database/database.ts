import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class DatabaseProvider {

  constructor(
    public sqlite: SQLite
  ) {
    console.log('Hello DatabaseProvider Provider');
  }

  
  public getDB() {
    return this.sqlite.create({
      name: 'kpeyes2.db',
      location: 'default'
    });
  }

  public createDatabase() {
    return this.getDB()
      .then((db: SQLiteObject) => {
        // Criando as tabelas
        this.createTables(db);
    
      })
      .catch(e => console.log(e));
  }

  private createTables(db: SQLiteObject) {
    // Criando as tabelas
    db.sqlBatch([        
      ['CREATE TABLE IF NOT EXISTS questionarios (idApi integer primary key, questionario VARCHAR(255), tipo VARCHAR(50))'],
      ['CREATE TABLE IF NOT EXISTS perguntas (idApi integer, idQuest integer, sigla VARCHAR(10), pergunta TEXT, peso integer, tipo integer,  PRIMARY KEY(idApi, idQuest) FOREIGN KEY(idQuest) REFERENCES questionarios(idApi))'],
      ['CREATE TABLE IF NOT EXISTS opcoesPerguntas (idPergunta integer primary key, opcao VARCHAR(50), FOREIGN KEY(idPergunta) REFERENCES perguntas(id))'],
      ['CREATE TABLE IF NOT EXISTS restaurantes (sigla VARCHAR(3) primary key NOT NULL, nome VARCHAR(255))'],
      ['CREATE TABLE IF NOT EXISTS respostas (id integer primary key AUTOINCREMENT NOT NULL, idQuest integer, idPergunta integer, sigla VARCHAR(3), idUser integer, dataResp TEXT, resposta TEXT, transmitido integer, FOREIGN KEY(idPergunta) REFERENCES perguntas(idApi), FOREIGN KEY(sigla) REFERENCES restaurantes(sigla))']
    ])
    .then(() => console.log('Tabelas criadas'))
    .catch(e => console.error('Erro ao criar as tabelas', e));

  }

}
