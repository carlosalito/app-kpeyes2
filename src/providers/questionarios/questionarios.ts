import { ServiceProvider } from './../service/service';
import { DatabaseProvider } from './../database/database';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import * as moment from 'moment';

@Injectable()
export class QuestionariosProvider {

  
  questionario = {
    restaurante : {
      sigla : null,      
      nome: '',
      id: null
    },
    questId: null,
    nome: null,
    usuario : null,
    data: moment().format('YYYY-MM-DD'),
    hora: moment().format('HH:mm'),
    perguntas: []
  };

  
  constructor(public http: HttpClient,
    public dbService : DatabaseProvider,
    public service: ServiceProvider    
    ) {
    console.log('Hello QuestionariosProvider Provider');
  }

  public insert(questionario) {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
      let sql = 'replace into questionarios (idApi, questionario, tipo) values (?, ?, ?)';
      let dados = [questionario.id, questionario.descricao, questionario.codigo];

      return db.executeSql(sql, dados)
      .catch((e) => console.error(e));  

    })
    .catch((e) => console.error(e));  
  }
  
  public getAll() {
    return this.dbService.getDB()
    .then((db : SQLiteObject) => {
      let sql = 'select * from questionarios';
      let dados = [];

      return db.executeSql(sql, dados)
        .then((data: any) => {
          if (data.rows.length > 0) {
            let questionarios = [];
            for (let i = 0; i < data.rows.length; i++) {
              const questionario = data.rows.item(i);
              questionarios.push(questionario);
            }
            return questionarios;
          } else {
            return [];
          }
        })
        .catch((e) => console.error(e));  

    })
    .catch((e) => console.error(e)); 
  }

  getQuestionarioApi() {
    return this.http.get(this.service.servidor + 'json&format=json&Itemid=806', {observe: 'response'});  
  }

}
