import { QuestionariosProvider } from './../../providers/questionarios/questionarios';
import { ServiceProvider } from './../../providers/service/service';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestaurantesProvider } from '../../providers/restaurantes/restaurantes';

@IonicPage()
@Component({
  selector: 'page-restaurantes',
  templateUrl: 'restaurantes.html',
})
export class RestaurantesPage implements OnInit {

  restaurantes :any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider,
    public restService: RestaurantesProvider,
    public questService: QuestionariosProvider    
  ) {
  }

  ngOnInit() {
    this.getRestaurantes();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RestaurantesPage');
  }

  getRestaurantes() {
    
    const loading = this.service.createLoading('carregando...');

    this.restService.getAll().then(data => {
      this.restaurantes = data;
      loading.dismiss();       
    })
    .catch((err: any) => {
      console.log(err);
      this.service.alerta('Erro ao carregar restaurantes', 'toast-error');      
      loading.dismiss();       
    });
  }

  setRestaurante(restaurante) {
      this.questService.questionario.restaurante.sigla = restaurante.sigla;
      this.questService.questionario.restaurante.nome = restaurante.nome;
      this.questService.questionario.restaurante.id = restaurante.id;
      this.navCtrl.push('QuestionariosPage');
  }

}
