import { LoginProvider } from './../../providers/login/login';
import { ServiceProvider } from './../../providers/service/service';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit{
  
  usuarioLogin =  {
    user : null,
    password: null
  }

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider,
    public loginService: LoginProvider,
    public storage: Storage,
    public menuCtrl: MenuController
  ) {
  }

  ngOnInit() {
    this.menuCtrl.swipeEnable(false);
    this.storage.get('usuario').then(user => {
      if (user) {
        this.iniciaAplicacao();
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  logar() {

    const loading = this.service.createLoading('logando...');
    
    const usuarioToLogin = [{
      username: this.usuarioLogin.user,
      password_clear: this.usuarioLogin.password
    }];

    this.loginService.logar(usuarioToLogin).subscribe(
      data => {
        console.log(data);
        if (data.body['response']) {
          this.storage.set('usuario', data.body);
          this.iniciaAplicacao();
        } else {
          this.service.alerta('Login inválido', '');
        }
      },
      err => {
        console.log(err);        
        loading.dismiss();
        this.service.alerta('Erro ao realizar login', '');
      },
      () => {
        loading.dismiss();
      }
    );
    
    
  }

  iniciaAplicacao() {
    this.menuCtrl.swipeEnable(true);
    this.navCtrl.setRoot('HomePage');
  }
}

