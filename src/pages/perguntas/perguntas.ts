import { QuestionariosProvider } from './../../providers/questionarios/questionarios';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PerguntasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-perguntas',
  templateUrl: 'perguntas.html',
})
export class PerguntasPage {


  pergunta : any;
  indexPergunta = 0;
  perguntas = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public questService: QuestionariosProvider
  ) {
    this.perguntas = this.questService.questionario.perguntas;
    this.pergunta = this.perguntas[this.indexPergunta];
    console.log(this.perguntas);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerguntasPage');
  }

  swipeEvent(event) {

    if (event.direction == 4) {
      if (this.indexPergunta > 0) {
        this.indexPergunta--;
      }
    } else if (event.direction == 2) {
      if (this.indexPergunta < this.perguntas.length - 1) {
        this.indexPergunta++;
      }
    }  

    this.pergunta = this.perguntas[this.indexPergunta];

    console.log(this.indexPergunta);
    

  }

}
