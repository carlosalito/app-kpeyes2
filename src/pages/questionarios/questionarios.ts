import { ServiceProvider } from './../../providers/service/service';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QuestionariosProvider } from '../../providers/questionarios/questionarios';
import { PerguntasProvider } from '../../providers/perguntas/perguntas';

@IonicPage()
@Component({
  selector: 'page-questionarios',
  templateUrl: 'questionarios.html',
})
export class QuestionariosPage implements OnInit{
  questionarios: any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider,
    public questService: QuestionariosProvider,
    public perguntaService: PerguntasProvider
  ) {
  }

  ngOnInit(){
    this.getQuestionarios();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuestionariosPage');
  }

  getQuestionarios() {
    
    const loading = this.service.createLoading('carregando...');

    this.questService.getAll().then(data => {
      this.questionarios = data;
      loading.dismiss();      
      console.log(this.questionarios);
       
    })
    .catch((err: any) => {
      console.log(err);
      this.service.alerta('Erro ao carregar questionários', 'toast-error');      
      loading.dismiss();       
    });
  }


  getQuestionario(questionario) {
    
    this.questService.questionario.questId = questionario.idApi;
    this.questService.questionario.nome = questionario.questionario;
    
    this.perguntaService.getQuestionarioPorId(questionario.idApi)
    .then((dados: any) => {
      this.questService.questionario.perguntas = dados;
      console.log(this.questService.questionario);    
      this.navCtrl.push('PerguntasPage');
    })
    .catch((e) => {
      console.log(e);
      this.service.alerta('Erro ao selecionar questionário', 'toast-error');      
    });    
  }

}
